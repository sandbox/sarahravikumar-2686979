INTRODUCTION
------------

This module provides a currency convert block.

-- INSTALLATION --

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

Once the module has been installed, navigate to 
admin/block-layout
(Configure the block in desired region).

MAINTAINERS
-----------

Current maintainers:
 * Saraswathi Ravikumar
