<?php
namespace Drupal\currency_convert\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Currency Convert Form.
 */
class CurrencyConvertForm extends FormBase {

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'currency_convert_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $currency_options = \Drupal::service('currency_convert.converter')->getCurrencyOptions();
    $form['currency_from_format'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select a currency to convert from'),
      '#options' => $currency_options,
    );
    $form['currency_to_format'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select a currency to convert'),
      '#options' => $currency_options,
    );
    $form['currency_convert_amount'] = array(
      '#type' => 'number',
      '#description' => $this->t('Please enter an amount'),
      '#maxlength' => 255,
      '#default_value' => $form_state->getValue('currency_convert_amount'),
    );
    $form['submission'] = array(
      '#prefix' => '<div>',
      '#markup' => $form_state->getValue('currency_convert_amount'),
      '#suffix' => '</div>',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('convert'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $currency_input = $form_state->getValue('currency_convert_amount');
    if (!empty($currency_input)) {
      $currency_from = $form_state->getValue('currency_from_format');
      $currency_to = $form_state->getValue('currency_to_format');
      $currency_output = \Drupal::service('currency_convert.converter')->currencyCheck($currency_from, $currency_to, $currency_input);
      $currency_output = t('The Converted Currency Value: @value', array('@value' => $currency_output));
      $form_state->setValue('currency_convert_amount', $currency_output);
      $form_state->setRebuild();
    }
  }

}
