<?php
namespace Drupal\currency_convert\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides my custom block.
 *
 * @Block(
 * id = "currency_convert",
 * admin_label = @Translation("Currency Convert Block"),
 * )
 */
class CurrencyConvertBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\currency_convert\Form\CurrencyConvertForm');
    return $form;
  }

}
