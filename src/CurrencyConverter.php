<?php
/**
 * @file
 * Contains \Drupal\currency_convert\CurrencyConverter.
 */
namespace Drupal\currency_convert;

use Drupal\Component\Serialization\Json;

/**
 * Currency Converter.
 */
class CurrencyConverter {

  /**
   * Get Currency data options.
   */
  public function getCurrencyOptions() { 
    return array(
      'ALL' => 'Albanian Lek',
      'DZD' => 'Algerian Dinar',
      'USD' => 'US Dollar',
      'EUR' => 'Euro',
      'XCD' => 'East Caribbean Dollar',
      'ARS' => 'Argentine Peso',
      'AWG' => 'Aruban Guilder',
      'AUD' => 'Australian Dollar',
      'BSD' => 'Bahamian Dollar',
      'BHD' => 'Bahraini Dinar',
      'BDT' => 'Bangladesh Taka',
      'BBD' => 'Barbados Dollar',
      'BYR' => 'Belarussian Ruble',
      'BZD' => 'Belize Dollar',
      'BMD' => 'Bermudian Dollar',
      'INR' => 'Indian Rupee',
      'BTN' => 'Bhutan Ngultrum',
      'BOB' => 'Boliviano',
      'BWP' => 'Botswana Pula',
      'NOK' => 'Norwegian Krone',
      'BRL' => 'Brazilian Real',
      'BND' => 'Brunei Dollar',
      'BGN' => 'Bulgarian Lev',
      'BIF' => 'Burundi Franc',
      'KHR' => 'Riel',
      'CAD' => 'Canadian Dollar',
      'CVE' => 'Cape Verde Escudo',
      'KYD' => 'Cayman Islands Dollar',
      'CLP' => 'Chilean Peso',
      'CNY' => 'Yuan Renminbi',
      'COP' => 'Colombian Peso',
      'KMF' => 'Comoro Franc',
      'NZD' => 'New Zealand Dollar',
      'CRC' => 'Costa Rican Colon',
      'HRK' => 'Croatian Kuna',
      'CUP' => 'Cuban Peso',
      'CZK' => 'Czech Koruna',
      'DKK' => 'Danish Krone',
      'DJF' => 'Djibouti Franc',
      'DOP' => 'Dominican Peso',
      'ECS' => 'Ecuador sucre',
      'EGP' => 'Egyptian Pound',
      'EQE' => 'Equatorial Guinean ekwele',
      'ETB' => 'Ethiopian Birr',
      'FKP' => 'Falkland Island Pound',
      'FJD' => 'Fiji Dollar',
      'FIM' => 'Finnish Markka',
      'FRF' => 'French Franc',
      'GMD' => 'Gambian Dalasi',
      'GHS' => 'Ghana Cedi',
      'GIP' => 'Gibraltar Pound',
      'GRD' => 'Greek Drachma',
      'GTQ' => 'Guatemala Quetzal',
      'GNF' => 'Guinea Franc',
      'GYD' => 'Guyana Dollar',
      'HTG' => 'Haiti Gourde',
      'HNL' => 'Honduras Lempira',
      'HKD' => 'Hong Kong Dollar',
      'HUF' => 'Hungarian Forint',
      'ISK' => 'Iceland Krona',
      'IRR' => 'Iranian Rial',
      'IQD' => 'Iraqi Dinar',
      'ILS' => 'New Israeli Sheqel',
      'JMD' => 'Jamaican Dollar',
      'JPY' => 'Japanese Yen',
      'JOD' => 'Jordanian Dinar',
      'KZT' => 'Kazakhstan Tenge',
      'KES' => 'Kenyan Shilling',
      'KWD' => 'Kuwaiti Dinar',
      'LVL' => 'Latvian Lats',
      'LBP' => 'Lebanese Pound',
      'LSL' => 'Lesotho Loti',
      'LRD' => 'Liberian Dollar',
      'LYD' => 'Libyan Dinar',
      'CHF' => 'Swiss Franc',
      'LTL' => 'Lithuanian Litas',
      'MOP' => 'Macau Pataca',
      'MKD' => 'Macedonian Denar',
      'MWK' => 'Malawi Kwacha',
      'MYR' => 'Malaysian Ringgit',
      'MVR' => 'Maldives Rufiyaa',
      'MTL' => 'Maltese Lira',
      'MRO' => 'Mauritania Ouguiya',
      'MUR' => 'Mauritius Rupee',
      'MXN' => 'Mexican Peso',
      'MNT' => 'Mongolian Tugrik',
      'MAD' => 'Moroccan Dirham',
      'MMK' => 'Myanmar Kyat',
      'NAD' => 'Namibia Dollar',
      'NPR' => 'Nepalese Rupee',
      'NLG' => 'Netherlands Guilder',
      'NIO' => 'Nicaragua Cordoba Oro',
      'NGN' => 'Nigerian Naira',
      'OMR' => 'Rial Omani',
      'PKR' => 'Pakistan Rupee',
      'PAB' => 'Panama Balboa',
      'PGK' => 'Papua New Guinea Kina',
      'PYG' => 'Paraguayan Guarani',
      'PEN' => 'Peruvian Nuevo Sol',
      'PHP' => 'Philippine Peso',
      'PLN' => 'Polish Zloty',
      'TPE' => 'Portuguese Timorese escudo',
      'QAR' => 'Qatari Rial',
      'RON' => 'Romanian New Leu',
      'RUB' => 'Russian Ruble',
      'RWF' => 'Rwanda Franc',
      'SHP' => 'Saint Helena Pound',
      'STD' => 'Sao Tome Dobra',
      'SAR' => 'Saudi Riyal',
      'RSD' => 'Serbian Dinar',
      'CSD' => 'Serbian Dinar',
      'SCR' => 'Seychelles Rupee',
      'SLL' => 'Leone',
      'SGD' => 'Singapore Dollar',
      'SKK' => 'Slovak Koruna',
      'SBD' => 'Solomon Islands Dollar',
      'SOS' => 'Somali Shilling',
      'LKR' => 'Sri Lanka Rupee',
      'SDG' => 'Sudanese Pound',
      'SRD' => 'Surinam Dollar',
      'SEK' => 'Swedish Krona',
      'SYP' => 'Syrian Pound',
      'TWD' => 'New Taiwan Dollar',
      'TZS' => 'Tanzanian Shilling',
      'THB' => 'Baht',
      'TTD' => 'Trinidata and Tobago Dollar',
      'TND' => 'Tunisian Dinar',
      'TRY' => 'New Turkish Lira',
      'RUR' => 'Russian rubleA/97',
      'SUR' => 'Soviet Union ruble',
      'UGX' => 'Uganda Shilling',
      'UAH' => 'Hryvnia',
      'GBP' => 'Pound Sterling',
      'UYU' => 'Peso Uruguayo',
      'VUV' => 'Vanuatu Vatu',
      'VEF' => 'Venezuelan Bolivar',
      'VND' => 'Vietnam Dong',
      'YER' => 'Yemeni Rial',
      'ZMK' => 'Kwacha',
      'ZWD' => 'Zimbabwe Dollar',
    );
  }
  /**
   * Get currency converted data.
   */
  public function currencyCheck($currency_from, $currency_to, $currency_input) {
    $yql_base_url = 'http://query.yahooapis.com/v1/public/yql';
    $yql_query = 'select * from yahoo.finance.xchange where pair in ("' . $currency_from . $currency_to . '")';
    $yql_query_url = $yql_base_url . '?q=' . urlencode($yql_query);
    $yql_query_url .= '&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';
    try {
      $yql_session = curl_init($yql_query_url);
      curl_setopt($yql_session, CURLOPT_RETURNTRANSFER, TRUE);
      $json_data = curl_exec($yql_session);
    }
    catch (\Exception $e) {
    }
    if (!empty($json_data)) {
      $data = Json::decode($json_data);
      $currency_output = (float) $currency_input * $data['query']['results']['rate']['Rate'];
    }
    return $currency_output;
  }

}
